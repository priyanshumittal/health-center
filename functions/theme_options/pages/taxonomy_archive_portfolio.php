<div class="block ui-tabs-panel deactive" id="option-ui-id-16" >	
	<?php $current_options = get_option('hc_pro_options');
	if(isset($_POST['webriti_settings_save_16']))
	{	
		if($_POST['webriti_settings_save_16'] == 1) 
		{
			if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_gernalsetting_nonce_customization'],'webriti_customization_nonce_gernalsetting') )
			{  print 'Sorry, your nonce did not verify.';	exit; }
			else  
			{		
				$current_options['taxonomy_portfolio_list']=sanitize_text_field($_POST['taxonomy_portfolio_list']);				
				update_option('hc_pro_options',$current_options);
			}
		}	
		if($_POST['webriti_settings_save_16'] == 2) 
		{
			$current_options['taxonomy_portfolio_list']=2;			
					
			update_option('hc_pro_options',$current_options);
		}
	}  ?>
	<form method="post" id="webriti_theme_options_16">
		<div id="heading">
			<table style="width:100%;"><tr>
				<td><h2><?php _e('Taxonomy Archive Portfolio Setting','health');?></h2></td>
				<td><div class="webriti_settings_loding" id="webriti_loding_16_image"></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_16_success" ><?php _e('Options data successfully Saved','health');?></div>
					<div class="webriti_settings_massage" id="webriti_settings_save_16_reset" ><?php _e('Options data successfully reset','health');?></div>
				</td>
				<td style="text-align:right;">
					<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('16');">
					<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('16')" >
				</td>
				</tr>
			</table>	
		</div>		
		<?php wp_nonce_field('webriti_customization_nonce_gernalsetting','webriti_gernalsetting_nonce_customization'); ?>
		
		<div class="section">
			<h3><?php _e('Select Layout Portfolio Archive','health') ?></h3>
			<?php $taxonomy_portfolio_list = $current_options['taxonomy_portfolio_list']; ?>	
				<select name="taxonomy_portfolio_list" class="webriti_inpute" >					
					<option value="2" <?php selected($taxonomy_portfolio_list, '2' ); ?>>2</option>
					<option value="3" <?php selected($taxonomy_portfolio_list, '3' ); ?>>3</option>
					<option value="4" <?php selected($taxonomy_portfolio_list, '4' ); ?>>4</option>
				</select>
				<span class="explain"><?php _e('This setting will help you to set the layout of the individual portfolio category ie you can add the portfolio categories as a menu and render the selected layout.','health'); ?></span>	
		</div>
				
		<div id="button_section">
			<input type="hidden" value="1" id="webriti_settings_save_16" name="webriti_settings_save_16" />
			<input class="reset-button btn" type="button" name="reset" value="Restore Defaults" onclick="webriti_option_data_reset('16');">
			<input class="btn btn-primary" type="button" value="Save Options" onclick="webriti_option_data_save('16')" >
		</div>
	</form>
</div>
