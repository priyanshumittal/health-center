<!-- Slider -->
<?php 
$current_options = get_option('hc_pro_options');
$animation= $current_options['animation'];
$animationSpeed=$current_options['animationSpeed'];
$direction=$current_options['slide_direction'];
$slideshowSpeed=$current_options['slideshowSpeed'];
?>
<script>
jQuery(window).load(function() {
  // The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
	directionNav: false,
    animationLoop: false,
    slideshow: false,
    asNavFor: '#slider'
  });
   
  jQuery('#slider').flexslider({
	animation: "slide",
	animation: "<?php echo $animation; ?>",
	animationSpeed: <?php echo $animationSpeed; ?>,
	direction: "<?php echo $direction; ?>",
	slideshowSpeed: <?php echo $slideshowSpeed; ?>,
    useCSS: false,
  });
});
</script>
<div class="hc_slider">
		<?php 	
		$count_posts = wp_count_posts( 'healthcenter_slider')->publish;
		$args = array( 'post_type' => 'healthcenter_slider','posts_per_page' =>$count_posts); 	
		$slider = new WP_Query( $args );
		if( $slider->have_posts() )
		{ ?>
			<div id="slider" class="flexslider">
			<ul class="slides">	
			<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>
				<li><?php if(has_post_thumbnail()):?>
						<?php $defalt_arg =array('class' => "img-responsive"); ?>
						<?php the_post_thumbnail('home_slider', $defalt_arg); ?>
						<?php endif; ?>	
					<div class="slide-caption">
						<div class="slide-text-bg1"><h1><?php echo the_title(); ?></h1></div>
						<div class="slide-text-bg2"><span><?php echo get_post_meta( get_the_ID(),'slider_text', true ) ; ?></span></div>	
						<div class="slide-btn-area-sm">
						<?php  if(get_post_meta( get_the_ID(),'slider_button_text', true )) 
								{  ?>
						<a class="slide-btn-sm" href="<?php echo get_post_meta( get_the_ID(),'slider_button_link', true ); ?>" <?php  if(get_post_meta( get_the_ID(),'slider_button_target', true )) { echo "target='_blank'"; }  ?>>
						<?php echo get_post_meta( get_the_ID(),'slider_button_text', true ); ?>
						</a>
						</div>
						<?php } ?>
					</div>						
				</li>
			<?php endwhile; ?>
			</ul>
		</div>		
		<?php } else { ?>
		<div id="slider" class="flexslider">
			<ul class="slides">	
			<?php for($i=1; $i<=5; $i++) {  ?>
			<li><img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/default/slide<?php echo $i; ?>.png" class="img-responsive" />
				<div class="slide-caption">
						<div class="slide-text-bg1"><h1><?php _e('Heart Specialist','health'); ?></h1></div>
						<div class="slide-text-bg2"><span><?php _e('If you need a doctor for to consectetuer Lorem ipsum dolor, consectetur adipiscing elit. Ut volutpat eros  adipiscing elit Ut volutpat.','health'); ?></span></div>	
						<div class="slide-btn-area-sm"><a href="#" class="slide-btn-sm">Read More</a></div>		
					</div>
				</li>
			<?php } ?>				
			</ul>
		</div>		
		<?php } ?>
</div>	
<!-- /Slider -->